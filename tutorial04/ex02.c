#include <stdio.h>

// printing to stdout and stderr
int main(void) {
    fprintf(stdout, "Standard out\n");
    fprintf(stderr, "Standard error\n");
    return 0;
}
