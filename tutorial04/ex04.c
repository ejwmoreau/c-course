#include <stdio.h>
#include <stdlib.h>

// allocating memory and printing values
int main(int argc, char *argv[]) {
    int arraylen = *argv[1] - '0';
    printf("Length %d\n", arraylen);
    int* array = (int *) malloc(arraylen * sizeof(int));
    int total;

    for (int i = 0; i < arraylen; i++) {
        array[i] = (i+1) * (i+1);
        total = total + array[i];
    }
    printf("%d\n", total);


    free(array);
    return 0;
}
