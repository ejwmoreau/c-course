#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// reading lines of any length
int main(int argc, char *argv[]) {
    int buflen = 8;
    char *buf = (char *) malloc(buflen * sizeof(char));
    int len;
    int flag = 0;

    while (fgets(buf, buflen, stdin) != NULL) {
        len = strlen(buf) - 1;

        if (buf[len] != '\n' && buf[len] != '\0') {
            if (flag == 0) {
                printf("(%d) %s", len, buf);
                buflen = buflen * 2;
                buf = (char *) realloc(buf, buflen * sizeof(char));
                flag = 1;
            } else if (flag == 1) {
                printf("%s", buf);
                flag = 1;
            }
        } else if (flag == 1) {
            buf[len] = '\0';
            printf("%s\n", buf);
            flag = 0;
        } else {
            buf[len] = '\0';
            printf("(%d) %s\n", len, buf);
            flag = 0;
        }
    }

    free(buf);
    return 0;
}
