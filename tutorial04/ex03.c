#include <stdio.h>

char buf[1024];

// counts the number of lines in a file
int main(void) {
    int count = 0;
    while (fgets(buf, sizeof(buf), stdin) != NULL) {
        count++;
    }
    printf("%d\n", count);
    return 0;
}
