#include <stdio.h>
#include <string.h>

FILE *eFile;

// sends the wrong barcodes to errors.log with a message
void error(char bar[]) {
    char mes[80];
    
    eFile = fopen("errors.log", "a");
    
    strcat(mes, "Error: Wrong L ------ ");
    strcat(mes, bar);
    fputs(mes, eFile);
}

int main(int argc, char *argv[]) {
    // variables to be used (b = barcode, l = location, Buf = buffer, Loc = location)
    char lLoc[5];
    char bLoc[5];
    char lBuf[64];
    char bBuf[64];
    int lLen = 4;
    FILE *vFile;

    /* 
     * Checks for:
     * 1) Enough arguments given
     * 2) --locations argument given
     */
    if (argc != 3) {
        printf("Not enough arguments\n");
        return 0;
    } else if (strcmp(argv[1], "--locations") != 0) {
        printf("Add --locations argument!\n");
        return 0;
    }

    // opens file with locations
    vFile = fopen(argv[2], "r");
    if (vFile == NULL) {
        printf("Wrong file/location\n");
        return 0;
    }

    // opens an empty errors.log file
    eFile = fopen("errors.log", "a");
    fputs("Errors in given Barcodes and Locations:\n", eFile);
    fputs("Legend: S = Sum, L = Locations, F = Format\n", eFile);
    fclose(eFile);

    // runs as long as there are still barcodes to check
    while (fgets(bBuf, sizeof(bBuf), stdin) != NULL) {
        vFile = fopen(argv[2], "r");

        // substring of str -> loc (Location only)
        strncpy(bLoc, bBuf, lLen);
        bLoc[4] = '\0';

        // runs until end of 'valid locations'
        while (fgets(lBuf, sizeof(lBuf), vFile) != NULL) {

            // extracts the location
            strncpy(lLoc, lBuf, lLen);

            // checks the location with each entry in 'valid locations'
            if (strcmp(lLoc, bLoc) == 0) {
                printf("%s", bBuf);
                break;
            } else {
                error(bBuf);
            }
        }
    }

    fclose(vFile);
    return 0;
}
