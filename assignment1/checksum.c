#include <stdio.h>
#include <string.h>

FILE *eFile;

// prints errors to errors.log with a message
void error(char bar[]) {
    char mes[80];
    
    eFile = fopen("errors.log", "a");
    
    strcat(mes, "Error: Wrong S ------ ");
    strcat(mes, bar);
    fputs(mes, eFile);
}

int main(void) {
    // variables to be used (blen = Barcode length, buf = Buffer)
    int i;
    int total;
    int bLen = 12;
    char buf[64];

    // opens error file to write title and legend
    eFile = fopen("errors.log", "a");
    fputs("Errors in given Barcodes and Locations:\n", eFile);
    fputs("Legend: S = Sum, L = Location, F = Format\n", eFile);
    fclose(eFile);
    
    while (fgets(buf, sizeof(buf), stdin) != NULL) {
        total = 0;
        
        // adds up first 11 integers in barcode
        for (i = 0; i < bLen-1; i++) {
            total = total + (buf[i] - '0');
        }
        
        // gets the last digit of total
        total = total % 10;
        
        // checks whether valid checksum or not
        if (total == (buf[i] - '0')) {
            printf("%s", buf);
        } else {
            error(buf);
        }
    }

    return 0;
}
