#include <stdio.h>
#include <string.h>

// variables (r = raw, v = valid, b = barcodes)
int lLen = 4;
int bLen = 12;
FILE *eFile;

// prints errors to errors.log with a message
void error(char bar[], int err) {
    char mes[80];
    
    eFile = fopen("errors.log", "a");
    
    // certain message if certain error:
    // 0 -> Fail at isValid()
    // 1 -> Fail at both location and checkSum()
    // 2 -> Fail at location
    // 3 -> Fail at checkSum()
    if (err == 0) {
        strcat(mes, "Error: Wrong F ------ ");
        strcat(mes, bar);
        fputs(mes, eFile);
    } else if (err == 1) {
        strcat(mes, "Error: Wrong S & L -- ");
        strcat(mes, bar);
        fputs(mes, eFile);
    } else if (err == 2) {
        strcat(mes, "Error: Wrong L ------ ");
        strcat(mes, bar);
        fputs(mes, eFile);
    } else if (err == 3) {
        strcat(mes, "Error: Wrong S ------ ");
        strcat(mes, bar);
        fputs(mes, eFile);
    }
}

// checks the sum of first 11 integers to last digit of barcode
int checkSum(char buf[]) {
    int i = 0;
    int total = 0;

    for (i = 0; i < bLen-1; i++) {
        total = total + (buf[i] - '0');
    }
    
    total = total % 10;
        
    if (total != (buf[i] - '0')) {
        return 1;
    }

    return 0;
}

void check() {
    // variables to be used (b = barcode, l = location, Buf = buffer, Loc = location, v = valid)
    FILE *vbFile;
    FILE *vlFile;
    char lLoc[4];
    char bLoc[4];
    char lBuf[64];
    char bBuf[64];
    int sFlag = 0; // 1 if valid, 0 if invalid
    int lFlag = 0; // 1 if valid, 0 if invalid

    // opens and checks the valid location and barcode files
    vbFile = fopen("vbFile.txt", "r");
    vlFile = fopen("vlFile.txt", "r");
    if (vbFile == NULL) {
        printf("Couldn't find valid barcodes");
        return;
    } else if (vlFile == NULL) {
        printf("Couldn't find the location file");
        return;
    }

    while (fgets(bBuf, sizeof(bBuf), vbFile) != NULL) {
        vlFile = fopen("vlFile.txt", "r");
        sFlag = 0;
        lFlag = 0;

        // does all checkSum
        // - if correct, flag it
        if (checkSum(bBuf) == 0) {
            sFlag = 1;
        }

        // extract unchecked location from unchecked barcode
        strncpy(bLoc, bBuf, lLen);
        bLoc[4] ='\0';

        // extract each valid location and compare to unchecked location
        while (fgets(lBuf, sizeof(lBuf), vlFile)) {

            // extract location in particular format
            strncpy(lLoc, lBuf, lLen);
            lLoc[4] = '\0';

            // checks each valid location with unknown location
            if (strcmp(bLoc, lLoc) == 0) {
                lFlag = 1;
                break;
            }
        }

        // checks sFlag and lFlag
        // - if both valid, print to stdout
        // - if either is invalid, then send error with a specific error code (0, 1, 2)
        if (sFlag == 1 && lFlag == 1) {
            printf("%s", bBuf);
            if ((bBuf[bLen] - 0) != 10) {
                printf("\n");
            }
        } else if (sFlag == 0 && lFlag == 0) {
            error(bBuf, 1);
        } else if (lFlag == 0) {
            error(bBuf, 2);
        } else if (sFlag == 0) {
            error(bBuf, 3);
        }
    }

    fclose(vlFile);
    fclose(vbFile);
}

void isValid(FILE *file, int len) {
    FILE *nFile;
    char buf[64];
    int i;
    int n;
    int flag = 0;
    
    // checking whether for location or barcode
    if (len == 4) {
        nFile = fopen("vlFile.txt", "w");
    } else {
        nFile = fopen("vbFile.txt", "w");
    }

    while (fgets(buf, sizeof(buf), file) != NULL) {
        flag = 0;

        // checks each char in line if it's a digit
        for (i = 0; i < len; i++) {
            n = (buf[i] - '0');
            if (n > 9 || n < 0) {
                flag = 1;
            }
        }
        
        // checks the char after barcode/location
        // - if \n or NULL, then there is no overflow
        n = buf[i] - 0;
        if (n != 10 && n != 0) {
            flag = 1;
        }

        // flag == 0 if barcode/location is valid
        if (flag == 0) {
            fputs(buf, nFile);
        } else {
            error(buf, 0);
        }
    }

    fclose(nFile);
}

int main(int argc, char *argv[]) {    
    FILE *rlFile;

    // checks for:
    // - enough arguments given
    // - "--locations" argument given
    if (argc != 3) {
        printf("Not enough arguments\n");
        return 1;
    } else if (strcmp(argv[1], "--locations") != 0) {
        printf("Add --locations argument!\n");
        return 1;
    }

    // opens file and checks if file exists
    rlFile = fopen(argv[2], "r");
    if (rlFile == NULL) {
        printf("Wrong file/location\n");
        return 1;
    }

    // opens error file to write title and legend
    eFile = fopen("errors.log", "a");
    fputs("Errors in given Barcodes and Locations:\n", eFile);
    fputs("Legend: S = Sum, L = Location, F = Format\n", eFile);
    fclose(eFile);

    // checks if either file/input is invalid
    isValid(rlFile, lLen);
    isValid(stdin, bLen);

    // runs the checks on vbFile using vlFile
    check();

    fclose(rlFile);
    return 0;
}
