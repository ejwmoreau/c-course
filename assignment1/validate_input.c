#include <stdio.h>
#include <string.h>

FILE *locFile;
FILE *eFile;

// sends wrong barcodes to errors.log with a message
void error(char bar[]) {
    char mes[80];
    
    eFile = fopen("errors.log", "a");
    
    strcat(mes, "Error: Wrong F ------ ");
    strcat(mes, bar);
    fputs(mes, eFile);
}

// checks which lines have valid lines and prints valid lines only
void isValid(FILE *vFile, int len) {
    // variables to be used (n = ascii value of char, flag = if a line is valid or not)
    char buf[64];
    int i;
    int n;
    int flag = 0;

    while (fgets(buf, sizeof(buf), vFile) != NULL) {
        flag = 0;

        // checks each char in line to make sure it's an int
        for (i = 0; i < len; i++) {
            n = (buf[i] - '0');
            if (n > 9 || n < 0) {
                flag = 1;
            }
        }

        // checks if last char is a newline character
        if ((buf[len] - 0) != 10) {
            flag = 1;
        }

        // checks flag and prt
        // - flag == 0 if barcode/location
        // - print if barcode, send to file if location
        if (flag == 0 && len == 12) {
            printf("%s", buf);
        } else if (flag == 0 && len == 4) {
            fputs(buf, locFile);
        } else {
            error(buf);
        }
    }
}

int main(int argc, char *argv[]) {
    // variables to be used (bLen = barcode length, vLen = location length)
    FILE *vFile;
    int bLen = 12;
    int vLen = 4;

    // checks for:
    // - enough arguments given
    // - "--locations" argument given
    if (argc != 3) {
        printf("Not enough arguments\n");
        return 1;
    } else if (strcmp(argv[1], "--locations") != 0) {
        printf("Add --locations argument!\n");
        return 1;
    }

    // opens file and checks if the file exists
    vFile = fopen(argv[2], "r");
    locFile = fopen("validLocations.txt", "w");
    if (vFile == NULL) {
        printf("Wrong file/location\n");
        return 1;
    }
    
    //opens error file to write title and legend
    eFile = fopen("errors.log", "a");
    fputs("Errors in given Barcodes and Locations:\n", eFile);
    fputs("Legend: S = Sum, L = Locations, F = Format\n", eFile);
    fclose(eFile);

    // checks if either file/input is invalid
    isValid(vFile, vLen);
    isValid(stdin, bLen);

    fclose(vFile);
    fclose(locFile);
    return 0;
}
