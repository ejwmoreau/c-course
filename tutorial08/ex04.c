#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define NTHREADS 4
#define MAX 1000
#define DIV (MAX / NTHREADS)

/*
 * Adds up all numbers from 1 to MAX using NTHREADS number of threads
 */

typedef struct {
    int *array;
    int start, end;
    int answer;
} WorkerArgs;

void *worker(void *arg) {
    WorkerArgs *wargs = (WorkerArgs *) arg;
    wargs->answer = 0;
    for (int i = wargs->start; i < wargs->end; i++) {
        wargs->answer += wargs->array[i];
    }

    return NULL;
}

int main(void) {

    WorkerArgs wargs[NTHREADS];
    pthread_t threads[NTHREADS];
    int *array = malloc(MAX * sizeof(int));

    for (int i = 0; i < MAX; i++) {
        array[i] = i+1;
    }

    for (int i = 0; i < NTHREADS; i++) {
        wargs[i].array = array;
        wargs[i].start = i * DIV;
        wargs[i].end = (i + 1) * DIV;
        printf("Start: %d, End: %d\n", wargs[i].start, wargs[i].end);
        wargs[i].answer = 0;
    }
    
    for (int i = 0; i < NTHREADS; i++) {
        pthread_create(&threads[i], NULL, worker, (void *) &wargs[i]);
    }

    for (int i = 0; i < NTHREADS; i++) {
        pthread_join(threads[i], NULL);
    }

    int sum = 0;
    for (int i = 0; i < NTHREADS; i++) {
        sum += wargs[i].answer;
    }

    printf("Total Sum is %d\n", sum);
    free(array);
}
