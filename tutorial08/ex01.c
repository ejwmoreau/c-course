#include <stdio.h>
#include <stdlib.h>

#define SIZE 9

int intcompare0(const void *x, const void *y) {
    const int *a = x;
    const int *b = y;
    return (*a - *b);
}

int intcompare1(const void *x, const void *y) {
    const int *a = x;
    const int *b = y;
    return (*b - *a);
}

int intcompare2(const void *x, const void *y) {
    const int *a = x;
    const int *b = y;
    return (abs(*a) - abs(*b));
}

int main(void) {
    int num[SIZE] = {13, -1, -11, 5, -7, 0, 2, 9, 14};

    printf("Array Original:  ");
    for (int i = 0; i < SIZE; i++)
        printf(" %d", num[i]);
    printf("\n");

    qsort(&num[0], SIZE, sizeof(int), intcompare0);
    printf("Array Ascending: ");
    for (int i = 0; i < SIZE; i++)
        printf(" %d", num[i]);
    printf("\n");

    qsort(&num[0], SIZE, sizeof(int), intcompare1);
    printf("Array Descending:");
    for (int i = 0; i < SIZE; i++)
        printf(" %d", num[i]);
    printf("\n");

    qsort(&num[0], SIZE, sizeof(int), intcompare2);
    printf("Array Absolute:  ");
    for (int i = 0; i < SIZE; i++)
        printf(" %d", num[i]);
    printf("\n");

    return 0;
}
