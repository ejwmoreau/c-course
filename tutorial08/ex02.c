#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFSIZE 1024
#define ARRSIZE 20

typedef struct {
    char unikey[9];
    int mark;
} MarkRow;

int markcmp(const void *va, const void *vb) {
    const MarkRow *a = va;
    const MarkRow *b = vb;
    return b->mark - a->mark;
}

int rowcmp(const void *va, const void *vb) {
    const MarkRow *a = va;
    const MarkRow *b = vb;
    if (a->mark == b->mark)
        return strcmp(a->unikey, b->unikey);
    return b->mark - a->mark;
}

int main(void) {
    MarkRow* rows = malloc(BUFSIZE * sizeof(MarkRow));
    int i = 0;

    while (2 == scanf("%s %d", rows[i].unikey, &rows[i].mark)) {
        i++;
    }

    printf("Scores Original:\n");
    for (int i = 0; i < ARRSIZE; i++)
        printf("%s %d\n", rows[i].unikey, rows[i].mark);
    printf("\n");

    qsort(rows, ARRSIZE, sizeof(MarkRow), rowcmp);
    printf("Scores Ascending:\n");
    for (int i = 0; i < ARRSIZE; i++)
        printf("%s %d\n", rows[i].unikey, rows[i].mark);
    printf("\n");

    int mark = 98;
    MarkRow tmp = {"aaaa1111", mark};
    MarkRow *result = bsearch(&tmp, rows, BUFSIZE, sizeof(MarkRow), markcmp);
    if (result == NULL) {
        printf("No one with a mark of %d\n", mark);
    } else {
        printf("%s has a mark of %d\n", result->unikey, result->mark);
    }
}
