#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

/*
 * Forking a new process and executing separate instructions in each process
 */

int main(void) {
    int n = 6;
    pid_t pid;

    printf("Before fork\n");
    pid = fork();
    if (pid < 0) {
        perror("Fork failed");
        return 1;
    }

    // Depending on whether the current process if the child or the parent, do something
    if (pid == 0) {
        n++;
        sleep(1);
    } else {
        n *= 2;
        wait(NULL);
    }

    printf("Fork returned %d, n is %d\n", pid, n);
    return 0;
}
