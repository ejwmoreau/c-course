#include <stdio.h>
#include <unistd.h>

/*
 * Launch another executable using execl()
 */

int main(void) {
    printf("About to launch /usr/bin/sort\n");

    if (execl("/usr/bin/sort", "sort", "forkdemo.c", (char *)NULL) < 0) {
        perror("Exec failed");
        return 1;
    }

    printf("Sort finished.\n");
    return 0;
}
