#include <stdio.h>
#include <inttypes.h>

/*
 * Program playing with unions and 2's complement
 */

union int8bits {
    int8_t val;
    struct {
        uint8_t rest : 7;
        uint8_t msb  : 1;
    } parts;
};

// turns on the most significant bit
void turn(int8_t val) {
    union int8bits num;
    num.val = val;

    if (num.parts.msb == 0) {
        num.parts.msb = 1;
    }

    printf("Old Number: %4d, New Number: %4d\n", val, num.val);
}

int main() {
    union int8bits ibits;
    
    for (ibits.val = -3; ibits.val <= 3; ibits.val++) {
        printf("Value: %4d, First: %d, Rest: %3d\n", ibits.val, ibits.parts.msb, ibits.parts.rest);
    }

    turn(5);
    turn(10);
    turn(-20);

    union int8bits num;
    num.val = 127;
    printf("Before adding 1 to 127 -> First: %d, Rest: %3d\n", num.parts.msb, num.parts.rest);
    num.val += 1;
    printf("After  adding 1 to 127 -> First: %d, Rest: %3d\n", num.parts.msb, num.parts.rest);

    return 0;
}
