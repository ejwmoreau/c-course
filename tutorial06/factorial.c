#include <stdio.h>

/*
 * Calculates factorial (used with gdb for debugging)
 * - use flag -g to allow debugging
 */

int factorial(const int n) {
    int result = 1;
    if (n > 0) {
        result = n * factorial(n - 1);
    }
    return result;
}

int main(void) {
    printf("%d\n", factorial(4));
    return 0;
}
