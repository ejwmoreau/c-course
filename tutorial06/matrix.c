#include <stdio.h>

/*
 * Program to perform some basic matrix multiplication
 */

void matrix_vector_mult(int matrix[3][3], int ivector[3], int ovector[3]);
void print_matrix(int matrix[3][3]);
void print_vector(int vector[3]);

int main(int argc, char* argv[]) {
    int matrix[3][3] = {
        {1, 0, 1},
        {0, -1, -2},
        {-1, 1, 0}
    };

    int vector[3] = {1, 1, 1};

    int ovector[3] = {0, 0, 0};

    matrix_vector_mult(matrix, vector, ovector);
    return 0;
}

void matrix_vector_mult(int matrix[3][3], int ivector[3], int ovector[3]) {
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            ovector[i] += (matrix[i][j] * ivector[j]); 
        }
    }

    printf("Matrix:\n");
    print_matrix(matrix);
    printf("Vector:\n");
    print_vector(ivector);
    printf("Product of Matrix and Vector:\n");
    print_vector(ovector);
}

void print_matrix(int matrix[3][3]) {
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            printf("%2d ", matrix[i][j]);
        }
        printf("\n");
    }
}

void print_vector(int vector[3]) {
    for (int i = 0; i < 3; i++) {
        printf("%d\n", vector[i]);
    }
}
