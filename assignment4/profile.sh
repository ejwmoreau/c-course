#!/bin/bash
gcc -O0 -std=gnu11 -g -pg -lm -lpthread pagerank.c -o pagerank

# run the program
time ./pagerank < tests/test12.in > /dev/null

# generate profile line by line
gprof -l ./pagerank > ln.stats
