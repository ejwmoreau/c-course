#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#include "pagerank.h"

void printAll(list* plist, double* scores);

inline void printAll(list* plist, double* scores) {
    for (node* i = plist->head; i != NULL; i = i->next) {
        printf("%s %.4lf\n", i->page->name, scores[i->page->index]);
    }
}

void pagerank(list* plist, int ncores, int npages, int nedges, double dampener) {

    // stores all scores
    double currScores[npages];
    double nextScores[npages];
    
    // pointers to scores
    double* scores0 = &currScores[0];
    double* scores1 = &nextScores[0];
    double* temp = 0;

    // stores all pages that have an inlink
    int pagesInLinks[npages];
    int pagesCount = 0;
    
    // stores pointers to pages
    page* pages[npages];

    // values used (stay the same)
    double initScores = (double) (1 - dampener) / npages;
    double startScores = (double) 1 / npages;

    // values used (to be changed)
    double sum = 0;
    double out_fac = 0;
    int index = 0;
    int i = 0;

    node* curr = plist->head;

    // initialize all scores (1/N) and first calculations
    for (i = 0; i < npages; i++) {
        pages[i] = curr->page;
        
        if (pages[i]->inlinks != NULL) {
            pagesInLinks[pagesCount] = i;
            pagesCount++;
            scores1[i] = 0;
        } else {
            scores0[i] = initScores;
            scores1[i] = initScores;
            sum += (initScores - startScores) * (initScores - startScores);
        }

        curr = curr->next;
    }

    // Check the initial scores and scores of first loop, etc
    for (i = 0; i < pagesCount; i++) {
        index = pagesInLinks[i];
        out_fac = 0;

        for (curr = pages[index]->inlinks->head; curr != NULL; curr = curr->next) {
            out_fac += startScores / pages[curr->page->index]->noutlinks;
        }

        scores0[index] = initScores + (dampener * out_fac);
        sum += (scores0[index] - startScores) * (scores0[index] - startScores);
    }
    
    // while it's not converged yet
    while (sum > EPSILON * EPSILON) {
        sum = 0;

        // change each P factor
        for (i = 0; i < pagesCount; i++) {
            index = pagesInLinks[i];
            out_fac = 0;

            for (curr = pages[index]->inlinks->head; curr != NULL; curr = curr->next) {
                out_fac += scores0[curr->page->index] / pages[curr->page->index]->noutlinks;
            }

            scores1[index] = initScores + (dampener * out_fac);
            sum += (scores1[index] - scores0[index]) * (scores1[index] - scores0[index]);
        }

        temp = scores0;
        scores0 = scores1;
        scores1 = temp;
    }

    //printf("\nFinal Print:\n");
    printAll(plist, scores0);

}

/*
Ideas:
- Store all inlinks in one array, one after another (optional)
- Then store all indexes that have at least one inlink
- Loop through that instead of through all pages

- inline functions
- EPSILON * EPSILON
- malloc, not calloc
- iterate through list, not through i
*/

/*
######################################
### DO NOT MODIFY BELOW THIS POINT ###
######################################
*/

int main(void) {

    /*
######################################################
### DO NOT MODIFY THE MAIN FUNCTION OR HEADER FILE ###
######################################################
*/

    list* plist = NULL;

    double dampener;
    int ncores, npages, nedges;

    /* read the input then populate settings and the list of pages */
    read_input(&plist, &ncores, &npages, &nedges, &dampener);

    /* run pagerank and output the results */
    pagerank(plist, ncores, npages, nedges, dampener);

    /* clean up the memory used by the list of pages */
    page_list_destroy(plist);

    return 0;
}
