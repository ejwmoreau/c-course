#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#include "pagerank.h"

#define EPSILON2 (EPSILON * EPSILON)

void printAll(list* plist, double* scores, double diff);

inline void printAll(list* plist, double* scores, double diff) {
    for (node* i = plist->head; i != NULL; i = i->next) {
        printf("%s %.4lf\n", i->page->name, (scores[i->page->index] + diff));
    }
}

void pagerank(list* plist, int ncores, int npages, int nedges, double dampener) {

    // stores all scores
    double* currScores = (double *) malloc(npages * sizeof(double));
    double* nextScores = (double *) malloc(npages * sizeof(double));
    
    // pointers to scores
    double* scores0 = &currScores[0];
    double* scores1 = &nextScores[0];
    double* temp = 0;

    // stores all pages that have an inlink
    int pagesInLinks[npages][3];
    int pagesCount = 0;
    int edgesCount = 0;

    // stores pointers to pages
    page* pages[npages];
    page* inlists[nedges];

    // values used (stay the same)
    npages = (double) npages;
    double initScores = (1 - dampener) / npages; // TODO remove all cases of this, except to print
    double startScores = 1 / npages;
    double diffScores = startScores - initScores; // TODO change to be subtracted from init

    // values used (to be changed)
    double sum = 0;
    double out_fac = 0;
    int index = 0;
    int i = 0;
    int j = 0;

    node* curr = plist->head;

    // initialize all scores (1/N) and first calculations
    for (i = 0; i < npages; i++) {
        pages[i] = curr->page;
        
        if (pages[i]->inlinks != NULL) {
            pagesInLinks[pagesCount][0] = i;
            pagesCount++;
        } else {
            sum += diffScores * diffScores;
        }

        curr = curr->next;
    }

    // Check the initial scores and scores of first loop, etc
    for (i = 0; i < pagesCount; i++) {
        index = pagesInLinks[i][0];
        pagesInLinks[i][1] = edgesCount;
        out_fac = 0;

        for (curr = pages[index]->inlinks->head; curr != NULL; curr = curr->next) {
            inlists[edgesCount] = curr->page;
            out_fac += startScores / pages[curr->page->index]->noutlinks;
            edgesCount++;
        }

        pagesInLinks[i][2] = edgesCount;
        scores0[index] = (dampener * out_fac);
        sum += (scores0[index] - diffScores) * (scores0[index] - diffScores);
    }

    // while it's not converged yet
    while (sum > EPSILON2) {
        sum = 0;

        // change each P factor
        for (i = 0; i < pagesCount; i++) {
            index = pagesInLinks[i][0];
            out_fac = 0;

            for (j = pagesInLinks[i][1]; j < pagesInLinks[i][2]; j++) {
                out_fac += (scores0[inlists[j]->index] + initScores) / inlists[j]->noutlinks;
            }

            scores1[index] = (dampener * out_fac);
            sum += (scores1[index] - scores0[index]) * (scores1[index] - scores0[index]);
        }

        //printf("\nNew Loop:\nScores0:\n");
        //printAll(plist, scores0, initScores);
        //printf("Scores1:\n");
        //printAll(plist, scores1, initScores);

        temp = scores0;
        scores0 = scores1;
        scores1 = temp;
    }

    //printf("\nFinal Print:\n");
    for (curr = plist->head; curr != NULL; curr = curr->next) {
        printf("%s %.4lf\n", curr->page->name, (scores0[curr->page->index] + initScores));
    }

    free(currScores);
    free(nextScores);
}

/*
Ideas:
- Store all inlinks in one array, one after another (optional)
- Then store all indexes that have at least one inlink
- Loop through that instead of through all pages

- inline functions
- EPSILON * EPSILON
- malloc, not calloc
- iterate through list, not through i
*/

/*
######################################
### DO NOT MODIFY BELOW THIS POINT ###
######################################
*/

int main(void) {

    /*
######################################################
### DO NOT MODIFY THE MAIN FUNCTION OR HEADER FILE ###
######################################################
*/

    list* plist = NULL;

    double dampener;
    int ncores, npages, nedges;

    /* read the input then populate settings and the list of pages */
    read_input(&plist, &ncores, &npages, &nedges, &dampener);

    /* run pagerank and output the results */
    pagerank(plist, ncores, npages, nedges, dampener);

    /* clean up the memory used by the list of pages */
    page_list_destroy(plist);

    return 0;
}
