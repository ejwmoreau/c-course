#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#include "pagerank.h"

#define EPSILON2 25E-6

double scores0[50000];
double scores1[50000];

page pageOrig[50000];
//page pageEdge[50000];

// changed these two to global
double outlinks[50000];
char* pageName[50000];
double pageSize[50000];

void pagerank(list* plist, int ncores, int npages, int nedges, double dampener) {
    
    // stores all pages that have an inlink
    int pagesCount = 0;
    int edgeCount = 0;

    // values used (stay the same)
    double initScores = (double) (1 - dampener) / npages;
    double startScores = 1.0 / npages;
    double diffScores = startScores - initScores;

    // values used (to be changed)
    double sum = 0;
    double subt = 0;
    double out_fac = 0;
    int index = 0;
    int linkIndex = 0;
    int current = 0;

    node* curr = plist->head;
    node* edge;

    // change to page Edge and then optimise to only include index and outlinks

    // add page to page array, add page with inlink to inlink index array, else change the score
    for (int i = 0; i < npages; i++) {
        outlinks[i] = 1.0/curr->page->noutlinks;
        pageName[i] = curr->page->name;

        if (curr->page->inlinks != NULL) {
            // give page info to pageOrig
            pageOrig[pagesCount] = *(curr->page);
            // give inlinks list size
            pagesCount++;
        } else {
            sum += diffScores * diffScores;
        }

        curr = curr->next;
    }

    //edgeCount = 0;
    //for (int i = 0; i < npages; i++) {
    //    printf("%s %.4lf\n", pageName[i], (scores1[i] + initScores));
    //}
    // Check the initial scores and scores of first loop, etc
    for (int i = 0; i < pagesCount; i++) {
        index = pageOrig[i].index;
        out_fac = 0;

        pageSize[i] = pageOrig[i].inlinks->length;
        // add edge pages to pageEdge
        //for (edge = pageOrig[i].inlinks->head; edge != NULL; edge = edge->next) {
        //    pageEdge[edgeCount] = *(edge->page);
        //    edgeCount++;
        //    out_fac += startScores * outlinks[edge->page->index];
        //}
        //for (int j = edgeCount; j < edgeCount + pageSize[i]; j++) {
            //printf("");
            //printf("Using edge index %d\n", pageEdge[j].index);
            //out_fac += startScores * outlinks[pageEdge[j].index];
        //}
        //edgeCount += pageSize[i];

        for (curr = pageOrig[i].inlinks->head; curr != NULL; curr = curr->next) {
            out_fac += startScores * outlinks[curr->page->index];
        }
        //printf("Changing score for %s from %.4lf to %.4lf\n", pageName[index], scores0[index], (dampener * out_fac));

        scores0[index] = dampener * out_fac;
        subt = scores0[index] - diffScores;
        sum += subt * subt;
    }

    //printf("First loop done with sum %.8lf:\n", sum);
    //for (int i = 0; i < npages; i++) {
    //    printf("%s %.4lf\n", pageName[i], (scores1[i] + initScores));
    //}
    // combining for loops might be faster?

    // while it's not converged yet
    while (sum > EPSILON2) {
        sum = 0;
        current = 1;
        //edgeCount = 0;

        // change each P factor
        for (int i = 0; i < pagesCount; i++) {
            index = pageOrig[i].index;
            out_fac = 0;

            //for (int j = edgeCount; j < edgeCount + pageSize[i]; j++) {
            //    linkIndex = pageEdge[j].index;
            //    out_fac += (scores0[linkIndex] + initScores) * outlinks[linkIndex];
            //}
            //edgeCount += pageSize[i];

            for (curr = pageOrig[i].inlinks->head; curr != NULL; curr = curr->next) {
                linkIndex = curr->page->index;
                out_fac += (scores0[linkIndex] + initScores) * outlinks[linkIndex];
            }

            scores1[index] = dampener * out_fac;
            subt = scores1[index] - scores0[index];
            sum += subt * subt;
        }


        //printf("Other loop done with sum %.8lf:\n", sum);
        //for (int i = 0; i < npages; i++) {
        //    printf("%s %.4lf\n", pageName[i], (scores1[i] + initScores));
        //}

        if (sum <= EPSILON2) {
            break;
        }

        sum = 0;
        current = 0;
        //edgeCount = 0;

        // change each P factor
        for (int i = 0; i < pagesCount; i++) {
            index = pageOrig[i].index;
            out_fac = 0;

            //for (int j = edgeCount; j < edgeCount + pageSize[i]; j++) {
            //    linkIndex = pageEdge[j].index;
            //    out_fac += (scores1[linkIndex] + initScores) * outlinks[linkIndex];
            //}
            //edgeCount += pageSize[i];

            for (curr = pageOrig[i].inlinks->head; curr != NULL; curr = curr->next) {
                linkIndex = curr->page->index;
                out_fac += (scores1[linkIndex] + initScores) * outlinks[linkIndex];
            }

            scores0[index] = dampener * out_fac;
            subt = scores0[index] - scores1[index];
            sum += subt * subt;
        }
        //printf("Other loop done with sum %.8lf:\n", sum);
        //for (int i = 0; i < npages; i++) {
        //    printf("%s %.4lf\n", pageName[i], (scores0[i] + initScores));
        //}
    }

    //printf("\nFinal Print:\n");
    if (current == 0) {
        for (int i = 0; i < npages; i++) {
            printf("%s %.4lf\n", pageName[i], (scores0[i] + initScores));
        }
    } else {
        for (int i = 0; i < npages; i++) {
            printf("%s %.4lf\n", pageName[i], (scores1[i] + initScores));
        }
    }
}

/*
Ideas:
- Store all inlinks in one array, one after another (optional)
- Then store all indexes that have at least one inlink
- Loop through that instead of through all pages
*/

/*
######################################
### DO NOT MODIFY BELOW THIS POINT ###
######################################
*/

int main(void) {

    /*
######################################################
### DO NOT MODIFY THE MAIN FUNCTION OR HEADER FILE ###
######################################################
*/

    list* plist = NULL;

    double dampener;
    int ncores, npages, nedges;

    /* read the input then populate settings and the list of pages */
    read_input(&plist, &ncores, &npages, &nedges, &dampener);

    /* run pagerank and output the results */
    pagerank(plist, ncores, npages, nedges, dampener);

    /* clean up the memory used by the list of pages */
    page_list_destroy(plist);

    return 0;
}
