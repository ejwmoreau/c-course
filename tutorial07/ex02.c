#include <stdio.h>
#include <unistd.h>

/*
 * Uses pipes to print results of 'ls -l' to the replaced pipe 1
 * - have to create pipe before forking, otherwise won't print properly
 */
int main(void) {
    int pipefd[2];
    if (pipe(pipefd) < 0) {
        perror("Could not create pipe");
        return 1;
    }
    
    pid_t proc = fork();
    if (proc < 0) {
        perror("Could not fork");
        return 1;
    }

    if (proc == 0) {
        close(pipefd[0]);
        close(1);
        dup(pipefd[1]);
        close(pipefd[1]);
        if (execlp("ls", "ls", "-l", NULL) < 0) {
            perror("Could not exec\n");
            return 1;
        }
    }

    close(pipefd[1]);
    FILE* fp = fdopen(pipefd[0], "r");
    char buf[1024];
    int i = 0;
    while (fgets(buf, sizeof(buf), fp) != NULL) {
        printf("ls %d: %s", i++, buf);
    }

    return 0;
}
