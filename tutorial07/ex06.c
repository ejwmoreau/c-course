#include <stdio.h>
#include <inttypes.h>

union Floatbits {
    float f;
    struct {
        uint32_t mantissa : 23;
        uint32_t exponent : 8;
        uint32_t sign : 1;
    } parts;
};

int main(void) {
    float n[7] = {0, 1, 2, -1, 1.5, 1.75, (6E30)};

    for (int i = 0; i < 7; i++) {
        union Floatbits bits;
        bits.f = n[i];
        printf("Value: %f -> Sign Bit: %u -> Expoenent: %u -> Mantissa: %u\n", bits.f, bits.parts.sign, bits.parts.exponent, bits.parts.mantissa);
    }
}
