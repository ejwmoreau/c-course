#include <stdio.h>

#define MAX (1000000)

/*
 * Prints all numbers from 1 to 1 million and backwards
 * to check if the associative rule of additon applies
 */

int main(void) {
    float n1 = 0;
    float n2 = 0;

    for (int i = 1; i <= MAX; i++) {
        n1 += (float) i;
    }

    for (int i = MAX; i > 0; i--) {
        n2 += (float) i;
    }

    printf("From 1 to 1 million: %f", n1);
    printf("From 1 million to 1: %f", n2);
}
