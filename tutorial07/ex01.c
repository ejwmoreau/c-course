#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

/*
 * Forks a new process then uses a pipe to communicate between the two processes
 */

int main(void) {
    pid_t proc;
    int pipefd[2];

    proc = fork();
    if (proc < 0) {
        perror("Fork failed");
        return 1;
    } else if (pipe(pipefd) < 0) {
        perror("Could not create pipe");
        return 1;
    }

    if (proc == 0) {
        close(pipefd[1]);
        char msg[64];
        read(pipefd[0], msg, sizeof(msg));
        printf("Child: Nooooo!!!\n");

    } else {
        close(pipefd[0]);
        char msg[] = "Luke, I am your father!";
        printf("Parent: Sending \"%s\" to the child\n", msg);
        write(pipefd[1], msg, strlen(msg));
    }

    return 0;
}
