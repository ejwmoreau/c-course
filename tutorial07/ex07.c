#include <stdio.h>
#include <unistd.h>

/*
 * Uses pipes and forks to print results of 'sort <filename> | uniq'
 * - have to create pipe before forking, otherwise won't print properly
 */
int main(int argc, char* argv[]) {
    if (argc != 2) {
        perror("Wrong number of arguments");
        return 1;
    }

    int pipefd[2];
    int result = pipe(pipefd);
    pid_t proc = fork();

    if (result < 0) {
        perror("Could not create pipe");
        return 1;
    } else if (proc < 0) {
        perror("Could not fork");
        return 1;
    }

    if (proc == 0) {
        dup2(pipefd[1], 1);
        close(pipefd[0]);
        close(pipefd[1]);
        if (execlp("sort", "sort", argv[1], NULL) < 0) {
            perror("Could not exec into sort");
            return 1;
        }
    }

    dup2(pipefd[0], 0);
    close(pipefd[0]);
    close(pipefd[1]);
    if (execlp("uniq", "uniq", NULL) < 0) {
        perror("Could not exec into uniq");
        return 1;
    }

    return 0;
}

