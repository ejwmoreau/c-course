#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "smb.h"

#define SIZE(x) (int) (sizeof(x)/sizeof(x[0]))
#define PIPE_SIZE 4
#define PRINT_STATUS() printf("%d %d %s %d %d\n", cValues[1], cValues[0], cStage->name, pipeSize(cStage->pipes), cStage->ncoins);

// Function declarations
int start(char buf[256]);
int addStage(char buf[256]);
stage* searchStage(char* name);
int addPipe(char* buf);
int stageExists(char* text);
int move(char* buf);
int moveN();
int moveP(int num);
int moveC(int coins);
int moveW(int coins, char* out);
int pipeSize(stage** pipes);
int checkStatus();
int freeStages();
int error();
int won();
int lost();

// Variables to be used
int tValues[5] = {-1, -1, -1, -1, -1};  // seed, coins, minutes, stages, connections (t = total)
int cValues[2] = {0, 0};                // coins, minutes (c = current)
int stages = 0;                         // # of stages existing
int iCoins = 0;                         // initial coins
int iConnections = 0;                   // initial connections
stage* sStage; // start Stage
stage* eStage; // end Stage
stage* cStage; // current Stage
stage* pStage; // previous Stage

////////////////////////////////////////
// Main function
// - Runs through each line of input and calls the appropriate functions
////////////////////////////////////////
int main(void) {
    char buf[256];
    int moves = 0;

    // deals with the first line and stores values
    if (fgets(buf, sizeof(buf), stdin) != NULL) {
        start(buf);
    }

    // deals with all lines initialising a stage and adding pipes
    while (fgets(buf, sizeof(buf), stdin) != NULL) {
        if (stages < tValues[3]) {
            addStage(buf);
        } else if (iConnections < tValues[4]) {
            addPipe(buf);
        } else {
            break;
        }
    }

    // deals with start and final stage
    sStage = searchStage(strtok(buf, " "));
    eStage = searchStage(strtok(NULL, " \n"));

    // deals with all moves
    cStage = sStage;
    while (fgets(buf, sizeof(buf), stdin) != NULL) {
        moves += 1;
        move(buf);
    }
    
    if (moves != 0) {
        lost();
    } else {
        error();
    }
    return 0;
}

////////////////////////////////////////
// Stores initial values from first line
////////////////////////////////////////
int start(char buf[256]) {
    char* value;
    int i = 0;

    value = strtok(buf, " ");
    srand(atoi(value));

    // store each integer in the line
    while(value != NULL) {
        if (atoi(value) < 0 || i > 4) {
            error();
        }

        tValues[i] = atoi(value);
        value = strtok(NULL, " \n");
        i++;
    }

    if (tValues[4] == -1) {
        error();
    }

    cValues[1] = tValues[2];

    return 0;
}

////////////////////////////////////////
// Adds the given stage and all given info
////////////////////////////////////////
int addStage(char buf[256]) {
    stage *stg = (stage *) malloc(sizeof(stage));
    cStage = stg;

    // checks whether start stage or not
    if (stages != 0) {
        pStage->next = cStage;
    } else {
        sStage = stg;
    }

    stages += 1;
    // sorts out the name of the stage
    char* value;
    value = strtok(buf, " ");
    for (int i = 0; i < SIZE(stg->name); i++) {
        if (i < (int) strlen(value)) {
            stg->name[i] = value[i];
        } else {
            stg->name[i] = 0;
        }
    }

    // sorts out the number of pipes in the stage
    value = strtok(NULL, " ");
    if (atoi(value) < 0 || atoi(value) > 4) {
        error();
    }
    stg->npipes = atoi(value);

    // sorts out the number of coins in the stage
    value = strtok(NULL, " ");
    iCoins += atoi(value);
    if (iCoins < 0) {
        error();
    }
    stg->ncoins = atoi(value);

    // allocate memory for all pipe pointers
    stg->pipes = (stage **) malloc(sizeof(stage *) * PIPE_SIZE);
    for (int i = 0; i < PIPE_SIZE; i++) {
        stg->pipes[i] = 0;
    }

    // wraps around the linked list
    if (stages == tValues[3]) {
        cStage->next = sStage;
    }

    // changing values
    pStage = cStage;      

    return 0;
}

////////////////////////////////////////
// searches and returns the stage with the given name
////////////////////////////////////////
stage* searchStage(char* text) {
    if (text == NULL || sStage == NULL) {
        error();
    }

    pStage = sStage;
    cStage = sStage->next;

    for (int i = stages; i > 0; i--) {
        if (strcmp(pStage->name, text) == 0) {
            return pStage; 
        }
        if (pStage->next != NULL) {
            pStage = cStage;
            cStage = pStage->next;
        }
    }

    error();
    return 0;
}

////////////////////////////////////////
// adds the given pipe connection
////////////////////////////////////////
int addPipe(char* buf) {
    char* value = strtok(buf, " ");
    int one = 0;
    int two = 0;

    stage* pipe1 = searchStage(value);

    // makes sure the buf is a pipe connection
    value = strtok(NULL, " ");
    if (value[0] != '~') {
        error();
    }

    value = strtok(NULL, " \n");
    stage* pipe2 = searchStage(value);

    for (int i = 0; i < 4; i++) {
        if (pipe1->pipes[i] == 0 && one == 0) {
            pipe1->pipes[i] = pipe2;
            one = 1;
        } else if (pipe1->pipes[i] != 0 && !strcmp(pipe1->pipes[i]->name, pipe2->name)) {
            break;
        }   
        if (pipe2->pipes[i] == 0 && two == 0) {
            pipe2->pipes[i] = pipe1;
            two = 1;
        } else if (pipe2->pipes[i] != 0 && !strcmp(pipe2->pipes[i]->name, pipe1->name)) {
            break;
        }
    }

    if (one == 0 || two == 0) {
        error();
    }                    

    iConnections += 1;

    return 0;
}

////////////////////////////////////////
// Finds how many pipes are connected to the stage
////////////////////////////////////////
int pipeSize(stage** pipes) {
    for (int i = 0; i < PIPE_SIZE; i++) {
        if (pipes[i] == 0) {
            return i;
        }
    }
    return 4;
}

////////////////////////////////////////
// Checks if a stage with the given name already exists
////////////////////////////////////////
int stageExists(char* test) {
    stage* temp = sStage;
    stage* next = temp->next;
    
    for (int i = stages; i > 0; i--) {
        if (!strcmp(temp->name, test)) {
            error();
        }
        if (i != 1) {
            next = temp->next;
            temp = next;
        }
    }

    return 0;
}

////////////////////////////////////////
// Deals with the given move
////////////////////////////////////////
int move(char* buf) {
    char* value;

    PRINT_STATUS();    
    value = strtok(buf, " \n");
    if (strcmp(value, "next") == 0) {
        moveN();
    } else if (strcmp(value, "pipe") == 0) {
        value = strtok(NULL, " \n");
        moveP(atoi(value));
    } else if (strcmp(value, "collect") == 0) {
        value = strtok(NULL, " \n");
        moveC(atoi(value));
    } else if (strcmp(value, "wager") == 0) {
        value = strtok(NULL, " \n");
        moveW(atoi(value), strtok(NULL, " \n"));
    } else {
        error();
    }

    checkStatus();

    return 0;
}

////////////////////////////////////////
// Moves to the next stage (worth one minute)
////////////////////////////////////////
int moveN() {
    cStage = cStage->next;
    cValues[1] -= 1;
    return 0;    
}

////////////////////////////////////////
// Moves to the specified pipe (worth one minute)
////////////////////////////////////////
int moveP(int num) {
    if (num < 1 || num > pipeSize(cStage->pipes)) {
        error();
    }
    cStage = cStage->pipes[num - 1];
    cValues[1] -= 1;
    return 0;    
}

////////////////////////////////////////
// Collects the specified number of coins (worth one minute per coin)
////////////////////////////////////////
int moveC(int coins) {
    if (coins <= 0 || coins > cStage->ncoins) {
        error();
    }

    cValues[0] += coins;
    cStage->ncoins -= coins;
    cValues[1] -= coins;

    return 0;    
}

////////////////////////////////////////
// Makes wager with specified number of coins (worth one minute)
////////////////////////////////////////
int moveW(int coins, char* out) {
    int ran = rand() % 2;

    if (coins > cValues[0]) {
        error();
    }

    if ((!strcmp(out, "even") && !ran) || (!strcmp(out, "odd") && ran)) {
        cValues[0] += coins;
    } else {
        cValues[0] -= coins;
    }

    cValues[1] -= 1;
    return 0;    
}

////////////////////////////////////////
// Checks on the status of the player (coins, minutes, stage)
////////////////////////////////////////
int checkStatus() {
    if (cStage->name == eStage->name) {
        if (cValues[0] >= tValues[1]) {
            won();
        } else {
            lost();
        }
    } else if (cValues[1] <= 0) {
        lost();
    }

    return 0;    
}

////////////////////////////////////////
// Frees all the memory allocated for all stages
////////////////////////////////////////
int freeStages() {
    if (sStage == NULL) {
        return 0;
    }

    pStage = sStage;
    cStage = pStage->next;

    for (int i = stages; i > 0; i--) {
        free(pStage->pipes);
        free(pStage);
        if (i != 1) {
            cStage = pStage->next;
            pStage = cStage;
        }
    }

    return 0;
}

////////////////////////////////////////
// Runs the error steps
////////////////////////////////////////
int error() {
    printf("error\n");
    freeStages();
    exit(1);
}

////////////////////////////////////////
// Runs the won steps
////////////////////////////////////////
int won() {
    PRINT_STATUS();
    printf("won\n");
    freeStages();
    exit(0);
}

////////////////////////////////////////
// Runs the lost steps
////////////////////////////////////////
int lost() {
    PRINT_STATUS();
    printf("lost\n");
    freeStages();
    exit(0);
}
