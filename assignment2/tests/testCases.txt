Test Cases:

Name	Testing
in.1	Good initial, no moves
in.2	seed = negative
in.3	coins = negative
in.4	minutes = negative
in.5	stages = negative
in.6	connections = negative
in.7	stages = 0
in.8	minutes = 0
in.9	coins = 0
in.10	connections = 0

in.11	wrong tStages (higher)
in.12	wrong tStages (lower)
in.13	wrong tConns (higher)
in.14	wrong tConns (lower)

in.15	straight to finish
in.16	to finish, enough coins
in.17	run out of minutes
in.18	stop halfway game

in.19	collect string
in.20	collect too much
in.21	collect negative
in.22	collect zero

in.23	wager too many coins
in.24	name too long
in.25	
