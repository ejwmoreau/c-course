#include <stdio.h>
#include <pthread.h>

#define NTHREADS (4)
#define BIGNUM   (100000)

/*
 * Fixing race conditions using locking
 */

int counter = 0;
pthread_mutex_t lock;           

void *worker(void *arg) {
    for (int i = 0; i < BIGNUM; i++) {
        pthread_mutex_lock(&lock);
        counter++;
        pthread_mutex_unlock(&lock);
    }
    return NULL;
}

int main(void) {
    pthread_t threads[NTHREADS];
    pthread_mutex_init(&lock, NULL);

    for (int i = 0; i < NTHREADS; i++) {
        pthread_create(&threads[i], NULL, worker, NULL);
    }

    for (int i = 0; i < NTHREADS; i++)
        pthread_join(threads[i], NULL);

    printf("counter  = %d\n", counter);
    printf("expected = %d\n", NTHREADS * BIGNUM);

    pthread_mutex_destroy(&lock);
    return 0;
}
