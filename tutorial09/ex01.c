#include <stdio.h>
#include <time.h>

// shows the amount of time taken to execute a particular part of code
int main(void) {
    clock_t tick, tock;
    tick = clock();

    tock = clock();
    printf("Time elapsed: %f seconds\n", (double) (tock - tick) / CLOCKS_PER_SEC);
}
