#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <pthread.h>

#define NTHREADS (4)
#define ITERATIONS (100000000)
#define DIV (ITERATIONS / NTHREADS)

/*
 * Calculate an estimate of pi using threads (no locking required)
 */

typedef struct {
    unsigned int state;
    int start, end;
    int answer;
} WorkerArgs;

void *worker(void *arg) {
    WorkerArgs *wargs = (WorkerArgs *) arg;
    wargs->answer = 0;
    for (int i = wargs->start; i < wargs->end; i++) {
        double x = (double) rand_r(&wargs->state) / (double) RAND_MAX;
        double y = (double) rand_r(&wargs->state) / (double) RAND_MAX;
        if (x*x + y*y <= 1)
            wargs->answer++;
    }

    return NULL;
}

int main(void) {
    WorkerArgs wargs[NTHREADS];
    pthread_t threads[NTHREADS];

    for (int i = 0; i < NTHREADS; i++) {
        wargs[i].state = i;
        wargs[i].start = i * DIV;
        wargs[i].end = (i + 1) * DIV;
        printf("Start: %d, End: %d\n", wargs[i].start, wargs[i].end);
    }
    
    for (int i = 0; i < NTHREADS; i++) {
        pthread_create(&threads[i], NULL, worker, (void *) &wargs[i]);
    }

    for (int i = 0; i < NTHREADS; i++) {
        pthread_join(threads[i], NULL);
    }

    int sum = 0;
    for (int i = 0; i < NTHREADS; i++) {
        sum += wargs[i].answer;
    }

    double pi = ((double)sum / (double)ITERATIONS) * 4;
    printf("Estimated Pi: %.8f\n", pi);
    printf("Actual Pi   : %.8f\n", M_PI);
    return 0;
}
