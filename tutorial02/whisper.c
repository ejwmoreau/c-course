#include <stdio.h>
#include <ctype.h>

// takes in line by line of text and returns it in lowercase
int main(void) {
	int c;
	while ((c = getchar()) != EOF) {
		c = tolower(c);
		putchar(c);
	}

	return 0;
}
