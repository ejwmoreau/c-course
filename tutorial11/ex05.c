#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <pthread.h>

/*
 * Uses barriers to control when three robots will work and when they will need to stop
 * - Instead of repeating pthread_create and pthread_join multiple times
 */

pthread_barrier_t barr1;
pthread_barrier_t barr2;

void *robot_worker(void *arg) {
    for (;;) {
        pthread_barrier_wait(&barr1);

        char *msg = (char *)arg;
        puts(msg);

        pthread_barrier_wait(&barr2);
    }

    return NULL;
}

int main(void) {
    pthread_barrier_init(&barr1, NULL, 4);
    pthread_barrier_init(&barr2, NULL, 4);

    // The jobs for each robot arm to execute. We'll launch a worker
    // thread per job.
    char *wkrs[] = {
        "cutting",
        "welding",
        "painting"
    };
    int nrobots = sizeof(wkrs) / sizeof(wkrs[0]);
    pthread_t tids[nrobots];

    // Start the robot arms.
    for (int i = 0; i < nrobots; i++) {
        pthread_create(&tids[i], NULL, robot_worker, wkrs[i]);
    }

    for (;;) {
        pthread_barrier_wait(&barr1);
        // robots will work in here
        pthread_barrier_wait(&barr2);

        // Wait 750ms for the conveyor belt to advance.
        puts("Advancing conveyor belt...");
        usleep(750000);
        puts("Next work item ready.");
    }

    return 0;
}
