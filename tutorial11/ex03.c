#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define ITERATIONS (100000000)

/*
 * Individual threads are adding random numbers together
 * 
 * Program was slow due to false-sharing
 * - Therefore, an extra padding was added to the struct
 *   to total 64 bytes for the cache line (assuming)
 * - Much faster now due to padding
 */

// Thread arg struct.
typedef struct {
    unsigned int seed;
    unsigned int answer;
    char padding[56];
} Targ;

// Initialised by main()
int nthreads;

void *worker(void *arg) {
    Targ *targ = (Targ *)arg;
    targ->answer = 0;
    for (int i = 0; i < ITERATIONS/nthreads; i++) {
        targ->answer += rand_r(&targ->seed);
    }
    return NULL;
}

int main(int argc, char *argv[]) {
    // Read in the number of threads from the command line.
    if (argc != 2 || sscanf(argv[1], "%d", &nthreads) != 1) {
        fprintf(stderr, "usage: %s <nthreads>\n", argv[0]);
        return 1;
    }

    pthread_t tids[nthreads];
    Targ targs[nthreads];
    for (int i = 0; i < nthreads; i++) {
        targs[i].seed = i;
        pthread_create(&tids[i], NULL, worker, &targs[i]);
    }

    unsigned int total = 0;
    for (int i = 0; i < nthreads; i++) {
        pthread_join(tids[i], NULL);
        total += targs[i].answer;
    }

    printf("%u\n", total);
    return 0;
}
