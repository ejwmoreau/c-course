#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

#define NPHIL 5

/*
 * Solved the dining philosophers problem using semaphores instead of mutexes
 */

pthread_t threads[NPHIL];
sem_t forks[NPHIL], limit;

void fail() {
    printf("Could not lock a mutex.\n");
    exit(1);
}

// Each thread is a philosopher at the table.
void* worker (void * arg) {
    // ID of the philosopher (from 0 to NPHIL-1)
    int id = *(int *) arg;

    // IDs of their left and right fork.
    int left = id;
    int right = (id + 1) % NPHIL;

    for (;;) {
        sem_wait(&limit);
        sem_wait(&forks[left]);
        sem_wait(&forks[right]);

        // Om nom nom.
        printf("Philosopher %d is eating...\n", id);

        sem_post(&forks[left]);
        sem_post(&forks[right]);
        sem_post(&limit);

        // Thinking.
        printf("Philosopher %d is thinking...\n", id);
    }
}

int main(void) {
    int args[NPHIL];

    // Create mutexes
    for (int i = 0; i < NPHIL; i++) {
        sem_init(&forks[i], 0, 1);
    }
    sem_init(&limit, 0, NPHIL-1);

    // Start each philosopher dining. They never stop.
    for (int i = 0; i != NPHIL; i++) {
        args[i] = i;
        pthread_create(&threads[i], NULL, worker, &args[i]);
    }

    for (;;)
        sleep(1);

    return 0;
}
