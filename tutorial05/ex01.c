#include <stdio.h>
#include "ex01.h"

// using macros in the header file
int main(int argc, char **argv) {
    
    for(int i = 1; i <= LOOPBOUND; i++) {
        for(int j = 1; j <= LOOPBOUND; j++) {
            printf("%d", mymin(i, j));
        }
        printf("\n");
    }
    return 0;
}
