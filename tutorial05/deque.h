#ifndef DEQUE_H
#define DEQUE_H

struct Node {
    int data;
    struct Node *next, *prev;
};

struct Deque {
    struct Node *front, *back;
};

// Alias the struct Deque * to a short type (Deque) so that
// the caller is presented with a simple interface.
typedef struct Deque * Deque;
typedef struct Node * Node;

// Creates an empty deque.
Deque deque_create(void);

// Returns 1 if the deque is empty, 0 otherwise.
int deque_isempty(Deque);

// Pushes a new element onto the back of the deque.
void push_front(Deque, int);
void push_back(Deque, int);

// Removes and returns the element at the front of the deque.
// WARNING: only call this when the deque is not empty.
int pop_front(Deque);
int pop_back(Deque);

// Deletes any elements in the deque, then
// deletes the deque itself.
void deque_delete(Deque);


#endif //QUEUE_H
