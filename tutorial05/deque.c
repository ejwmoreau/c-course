#include <stdio.h>
#include <stdlib.h>

#include "deque.h"

/*
 * Implementation of a deque using structs
 */

Deque deque_create(void) {
    Deque q = (Deque) malloc(sizeof(struct Deque));
    q->front = NULL;
    q->back = NULL;
    return q;
}

int deque_isempty(Deque q) {
    if (q->front == NULL || q->back == NULL) {
        return 1;
    }    
    return 0;
}

void push_front(Deque q, int d) {
    Node n = (Node) malloc(sizeof(struct Node));
    n->data = d;

    if (deque_isempty(q)) {
        q->front = n;
        q->back = n;
    }

    n->next = q->front;
    q->front->prev = n;
    q->front = n;
    n->prev = NULL;
}

void push_back(Deque q, int d) {
    Node n = (Node) malloc(sizeof(struct Node));
    n->data = d;

    if (deque_isempty(q)) {
        q->front = n;
        q->back = n;
    }

    n->prev = q->back;
    q->back->next = n;
    q->back = n;
    n->next = NULL;
}

int pop_front(Deque q) {
    if (deque_isempty(q) == 1)
        return -1;

    Node n = q->front;
    int d = n->data;

    q->front = q->front->next;

    free(n);
    return d;
}

int pop_back(Deque q) {
    if (deque_isempty(q) == 1)
        return -1;

    Node n = q->back;
    int d = n->data;

    q->back = q->back->prev;
    q->back->next = NULL;

    free(n);
    return d;
}

void deque_delete(Deque q) {
    Node curr = q->front;
    Node next;

    while (curr != NULL) {
        next = curr->next;
        free(curr);
        curr = next;
    }
    free(q);
}

int main(void) {
    Deque q = deque_create();

    push_front(q, 1);
    push_front(q, 2);
    push_back(q, 3);

    printf("Node %d is popped\n", pop_front(q));

    push_back(q, 4);

    printf("Node %d is popped\n", pop_back(q));
    
    deque_delete(q);

    return 0;
}
