#include <stdio.h>
#include <stdlib.h>

#include "queue.h"

/*
 * Implementation of a queue using structs
 */

Queue queue_create(void) {
    Queue q = (Queue) malloc(sizeof(struct Queue));
    q->front = NULL;
    q->back = NULL;
    return q;
}

int queue_isempty(Queue q) {
    if (q->front == NULL || q->back == NULL) {
        return 1;
    }    
    return 0;
}

void queue_push(Queue q, int d) {
    Node n = (Node) malloc(sizeof(struct Node));
    n->data = d;

    if (queue_isempty(q)) {
        q->front = n;
        q->back = n;
    }

    n->prev = q->back;
    q->back->next = n;
    q->back = n;
    n->next = NULL;
}

int queue_pop(Queue q) {
    if (queue_isempty(q) == 1)
        return -1;

    Node n = q->front;
    int d = n->data;

    q->front = q->front->next;

    free(n);
    return d;
}

void queue_delete(Queue q) {
    Node curr = q->front;
    Node next;

    while (curr != NULL) {
        next = curr->next;
        free(curr);
        curr = next;
    }
    free(q);
}

int main(void) {
    Queue q = queue_create();

    queue_push(q, 1);
    queue_push(q, 2);
    queue_push(q, 3);

    printf("Node %d is popped\n", queue_pop(q));

    queue_push(q, 4);

    printf("Node %d is popped\n", queue_pop(q));
    
    queue_delete(q);

    return 0;
}
