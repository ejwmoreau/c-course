#include <stdio.h>

// Commands for C Compiler outputs
int main(void) {
    printf("----- C Compiler outputs -----\n");
    printf("Pre-processor only: gcc -E foo.c\n");
    printf("Assembly Program:   gcc -S foo.c\n");
    printf("Object Files:       gcc -c -o foo.o foo.s (Using the Assembly program\n");
    printf("View Object Files:  objdump -s foo.o\n");
}
