#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define NTHREADS 4

/*
 * Used barriers to parallelise the calculation of each row
 */

typedef struct {
    int *current;
    int *next;
    int start;
    int end;
    int width;
} Warg;

pthread_barrier_t barr1;
pthread_barrier_t barr2;
pthread_t pthreads[NTHREADS];
Warg wargs[NTHREADS];

// Prints the board, using '.' for a dead cell, and 'X' for a live cell.
void print_board(int *board, int width) {
    for (int i = 0; i < width; i++) {
        printf("%c", (board[i] == 0) ? '.' : 'X');
    }
    printf("\n");
}

void *worker(void *arg) {
    for (;;) {
        pthread_barrier_wait(&barr1);
        Warg *warg = (Warg *)arg;
        for (int i = warg->start; i < warg->end; i++) {
            int left = (i == 0) ? warg->width-1 : i-1;
            int right = (i == warg->width-1) ? 0 : i+1;
            warg->next[i] = warg->current[left] ^ warg->current[right];
        }
        pthread_barrier_wait(&barr2);
    }

    return NULL;
}

int main(void) {
    int width = 64;
    int div = width / NTHREADS;
    int generations = 33;
    int *board1 = calloc(width, sizeof(int));
    int *board2 = calloc(width, sizeof(int));
    pthread_barrier_init(&barr1, NULL, NTHREADS+1);
    pthread_barrier_init(&barr2, NULL, NTHREADS+1);

    for (int i = 0; i < NTHREADS; i++) {
        wargs[i].start = i * div;
        wargs[i].end = (i + 1) * div;
        printf("Start: %d, End: %d\n", wargs[i].start, wargs[i].end);
        pthread_create(&pthreads[i], NULL, worker, (void *) &wargs[i]);
        wargs[i].width = width;
    }

    if (wargs[NTHREADS-1].end != width) {
        wargs[NTHREADS-1].end = width;
    }

    // Initial generation: only has one live cell in the middle.
    board1[width/2] = 1;

    for (int i = 0; i < generations; i++) {
        // Print the current generation.
        printf("Generation %02d: ", i);
        print_board(board1, width);

        for (int i = 0; i < NTHREADS; i++) {
            wargs[i].current = board1;
            wargs[i].next = board2;
        }

        pthread_barrier_wait(&barr1);
        // workers calculate the next row here
        pthread_barrier_wait(&barr2);

        // Swap the current and previous generations.
        int *tmp = board1;
        board1 = board2; 
        board2 = tmp;
    }

    free(board1);
    free(board2);
    return 0;
}
