#include <stdio.h>

#define LEN 8000000

/*
 * Recursive functions can easily cause stack overflows
 * - Seg Fault at about 1,000,000 characters
 *
 * Iterative version can go up to about 8,000,000 characters before stack overflows
 */

int is_palin_rec(char *str, int len) {
    if (len <= 1)
        return 1;
    else if (str[0] != str[len-1])
        return 0;
    return is_palin_rec(str+1, len-2);
}

int is_palin_iter(char *str, int len) {
    int flag = 1;

    if (len <= 1)
        return flag;

    for (int i = 0; i < len/2; i++) {
        if (str[i] != str[len - i - 1]) {
            flag = 0;
            break;
        }
    }

    return flag;
} 

int main(void) {
    int ans = 0;
    int rec = 0;
    char word[LEN];

    for (int i = 0; i < LEN; i++) {
        word[i] = 'b';
    }

    if (rec == 1) {
        ans = is_palin_rec(&word[0], LEN);
    } else {
        ans = is_palin_iter(&word[0], LEN);
    }

    if (ans == 1) {
        printf("Palindrome\n");
    } else {
        printf("Not Palindrome\n");
    }
    return 0;
}
