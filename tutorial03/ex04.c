#include <stdio.h>

// scans an integer, long float and string, then prints them
int main (void) {
	int x;
	double y;
	char buf[20];
	if (scanf("%d %lf %s", &x, &y, buf) != 3) {
		fprintf(stderr, "incorrect input\n");
		return 1;
	}
	printf("%d %f %s\n", x, y, buf);
	return 0;
}
