#include <stdio.h>
#include <math.h>

#define MAXLEN (1024)

// returns the mean and standard deviation of all given numbers
int main (void) {
	double nums[MAXLEN];
	int pos = -1;
	
    while (scanf("%lf", &nums[++pos]) != EOF) {
        printf("Number is %.2f:\n", nums[pos]);
    }

	double mean = 0, stdev = 0;

	for (int i = 0; i < pos; i++) {
		mean += nums[i];
	}

	mean = mean / pos;

    for (int i = 0; i < pos; i++) {
        stdev += (nums[i] - mean) * (nums[i] - mean);
    }

    stdev = sqrt((1.0 / (pos - 1.0)) * stdev);

	printf("mean: %.2f\n", mean);
	printf("stdev: %.2f\n", stdev);
	return 0;
}
