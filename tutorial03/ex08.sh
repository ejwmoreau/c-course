#!/bin/sh

# Intro exercise to Shell scripts

echo 'Hello, World!'

total="0"

for cfile in *.c
do
    nlines=$(wc -l < $cfile)
    total=$(($total + $nlines))
done

echo "$total lines of code found."
