#include <stdio.h>

// scans a number given by the user
int main(void) {
	int x;
	if (scanf("%d", &x) != 1) {
		fprintf(stderr, "no number given\n");
		return 1;
	}
	printf("%d\n", x);
	return 0;
}
