#include <stdio.h>
#include <stdlib.h>

// prints an integer, float and string
int main(void)
{
	printf("integer=%d float=%f string=%s\n", 4711, 3.141, "Hello, World!");
	return 0;
}
