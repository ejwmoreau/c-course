#include <stdio.h>

/* 
 * Exercise with structs and printing from structs
 */

struct Pokemon {
	char nickname[11];
	char type[11];
	int level;
};
typedef struct Pokemon Pokemon;

void printPokemon(Pokemon* poke);

void printPokemon(Pokemon* poke) {
    printf("New Pokemon:\n");
	printf("    Name: %s\n    Type: %s\n    Level: %d\n", poke->nickname, poke->type, poke->level);
}

int main(void) {
    Pokemon first = {"Bill", "Bulbasaur", 3};
	printPokemon(&first);
}
