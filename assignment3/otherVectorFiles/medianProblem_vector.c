#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <stdbool.h>
#include <pthread.h>
#include <inttypes.h>

#include "vector.h"

#define NUMS_MAX 10000000

static int64_t g_seed = 0;
static int64_t g_length = 0;

int64_t* nums;
int64_t primesMax = 2;

int64_t medianIndex = 0;

////////////////////////////////
///     UTILITY FUNCTIONS    ///
////////////////////////////////

/**
 * Creates an array of 0 to NUMS_MAX numbers
 * - Value is either 0 (for prime) and 1 (for non-prime)
 */
void primeSieve(void) {
    nums = calloc(NUMS_MAX, sizeof(int64_t));

    int64_t i = 0;

    for (i = 2; i*i <= NUMS_MAX; i++) {
        if (nums[i] == 0) {
            for (int64_t j = i*i; j < NUMS_MAX; j += i) {
                nums[j] = 1;
            }
        }
    }
}

/**
 * Frees all the primes in Prime Sieve
 */
void freePrimes(void) {
    
    free(nums);
}

/**
 * Sets the number of elements that each vector will contain
 */
void set_length(int64_t length) {

    g_length = length;
    medianIndex = (g_length - 1) / 2;
}

/**
 * Sets the seed used when generating pseudorandom numbers
 */
void set_seed(int64_t seed) {

    g_seed = seed;
}

/**
 * Returns pseudorandom number determined by the seed
 */
int64_t fast_rand(void) {

    g_seed = (214013 * g_seed + 2531011);
    return (g_seed >> 16) & 0x7FFF;
}

////////////////////////////////
///   VECTOR INITALISATIONS  ///
////////////////////////////////

/**
 * Returns new vector, with all elements set to zero
 */
int64_t* new_vector(void) {

    return (int64_t *) calloc(g_length, sizeof(int64_t));
}

/**
 * Returns new vector, with all elements set to given value
 */
int64_t* uniform_vector(int64_t value) {

    int64_t* vector = new_vector();
    int64_t i = 0;

    for (i = 0; i < g_length; i++) {
        vector[i] = value;
    }

    return vector;
}

/**
 * Returns new vector, with elements generated at random using given seed
 */
int64_t* random_vector(int64_t seed) {

    int64_t* vector = new_vector();
    int64_t i = 0;

    set_seed(seed);

    for (i = 0; i < g_length; i++) {
        vector[i] = fast_rand();
    }

    return vector;
}

/**
 * Returns whether given number is prime
 */
int64_t is_prime(int64_t number) {

    if (number < NUMS_MAX) return !nums[number];
    else if (!(number % 2) || !(number % 3) || !(number % 5)) return 0;

    int64_t i = 0;

    for (i = 5; i*i <= number; i += 6) {
        if (!(number % i) || !(number % (i+2))) {
            return 0;
        }
    }

    return 1;
}

/**
 * Returns new vector, containing primes numbers in sequence from given start
 */
int64_t* prime_vector(int64_t start) {

    int64_t* vector = new_vector();
    int64_t i = 0;

    while (i < g_length) {

        if (is_prime(start)) {
            vector[i] = start;
            i += 1;
        }

        start += 1;
    }

    return vector;
}

/**
 * Returns new vector, with elements in sequence from given start and step
 */
int64_t* sequence_vector(int64_t start, int64_t step) {

    int64_t* vector = new_vector();
    int64_t i = 0;

    for (i = 0; i < g_length; i++) {
        vector[i] = start;
        start += step;
    }

    return vector;
}

////////////////////////////////
///     VECTOR OPERATIONS    ///
////////////////////////////////

/**
 * Returns new vector, cloning elements from given vector
 */
int64_t* cloned(int64_t* vector) {

    int64_t* clone = new_vector();
    int64_t i = 0;

    for (i = 0; i < g_length; i++) {
        clone[i] = vector[i];
    }

    return clone;
}

/**
 * Returns new vector, with elements ordered in reverse
 */
int64_t* reversed(int64_t* vector) {

    int64_t* result = new_vector();
    int64_t i = 0;

    for (i = 0; i < g_length; i++) {
        result[i] = vector[g_length - 1 - i];
    }

    return result;
}

/**
 * Comparison in ascending order
 */
int intcompareAsc(const void* a, const void* b) {
    if (*(int64_t*)a < *(int64_t*)b) return -1;
    else if (*(int64_t*)a == *(int64_t*)b) return 0;
    else return 1;
}

/**
 * Comparison in descending order
 */
int intcompareDesc(const void* a, const void* b) {
    if (*(int64_t*)a > *(int64_t*)b) return -1;
    else if (*(int64_t*)a == *(int64_t*)b) return 0;
    else return 1;
}

/** TODO
 * If flag==0, returns ascending order
 * If flag==1, returns descending order
 */
void sort(int64_t* vector, int64_t flag) {
    if (g_length <= 1) return;

    if (flag == 0) {
        qsort(vector, g_length, sizeof(int64_t), intcompareAsc);
    } else {
        qsort(vector, g_length, sizeof(int64_t), intcompareDesc);
    }
}

/** TODO
 * Returns new vector, with elements ordered from smallest to largest
 */
int64_t* ascending(int64_t* vector) {
    int64_t* result = cloned(vector);

    sort(result, 0);

    return result;
}

/** TODO
 * Returns new vector, with elements ordered from largest to smallest
 */
int64_t* descending(int64_t* vector) {
    int64_t* result = cloned(vector);
    
    sort(result, 1);

    return result;
}

/**
 * Returns new vector, adding scalar to each element
 */
int64_t* scalar_add(int64_t* vector, int64_t scalar) {

    int64_t* result = new_vector();
    int64_t i = 0;

    for (i = 0; i < g_length; i++) {
        result[i] = vector[i] + scalar;
    }

    return result;
}

/**
 * Returns new vector, multiplying scalar to each element
 */
int64_t* scalar_mul(int64_t* vector, int64_t scalar) {

    int64_t* result = new_vector();
    int64_t i = 0;

    for (i = 0; i < g_length; i++) {
        result[i] = vector[i] * scalar;
    }

    return result;
}

/**
 * Returns new vector, adding elements with the same index
 */
int64_t* vector_add(int64_t* vector_a, int64_t* vector_b) {

    int64_t* result = new_vector();
    int64_t i = 0;

    for (i = 0; i < g_length; i++) {
        result[i] = vector_a[i] + vector_b[i];
    }

    return result;
}

/**
 * Returns new vector, multiplying elements with the same index
 */
int64_t* vector_mul(int64_t* vector_a, int64_t* vector_b) {

    int64_t* result = new_vector();
    int64_t i = 0;

    for (i = 0; i < g_length; i++) {
        result[i] = vector_a[i] * vector_b[i];
    }

    return result;
}

////////////////////////////////
///       COMPUTATIONS       ///
////////////////////////////////

/**
 * Returns the sum of all elements
 */
int64_t get_sum(int64_t* vector) {

    int64_t sum = 0;
    int64_t i = 0;

    for (i = 0; i < g_length; i++) {
        sum += vector[i];
    }

    return sum;
}

/** TODO
 * Returns the most frequently occuring element
 * or -1 if there is no such unique element
 *
 * [1 2 2] => 2
 * [1 2 3] => -1
 */
int64_t get_mode(int64_t* vector) {
    if (g_length <= 1) {
        return vector[0];
    }

    int64_t* sorted = cloned(vector);
    int64_t savedItem = -1;
    int64_t savedCount = 0;
    int64_t currItem = -1;
    int64_t currCount = 0;
    int64_t duplicate = 0;
    int64_t i = 0;

    sort(sorted, 0);

    for (i = 0; i < g_length; i++) {
        if (currItem == sorted[i]) {
            currCount++;
        } else if (currItem != sorted[i]) {
            if (currCount > savedCount) {
                savedCount = currCount;
                savedItem = currItem;
                duplicate = 0;
            } else if (currCount == savedCount) {
                duplicate = 1;
            }

            currCount = 0;
            currItem = sorted[i];
        }
    }

    free(sorted);

    if (duplicate == 1) {
        return -1;
    } else {
        return savedItem;    
    }
}

int64_t findPivot(int64_t* vector, int64_t n) {
    int64_t c = 5;
    int64_t pos = 0;
    int64_t tmp = 0;
    int64_t end = 0;
    int64_t start = 0;
    int64_t i = 0;
    int64_t j = 0;

    while (n > 1) {
        pos = 0;
        tmp = 0;

        for (start = 0; start < n; start += c) {

            end = start + c;
            if (end > n)
                end = n; ///

            //printf("\nElements: ");
            for (i = start; i < end-1; i++) {
                //printf("(%ld -> ", vector[i]);
                for (j = i + 1; j < end; j++) {
                    if (vector[j] < vector[i]) {
                        tmp = vector[i];
                        vector[i] = vector[j];
                        vector[j] = tmp;
                    }
                }
                //printf("%ld)\n", vector[i]);
            }

            //printf("Start: %ld, Median: %ld, End: %ld\n", start, (start+end-1)/2, end);
            end = (start + end) / 2;
            //printf("Median: %ld\n", vector[end]);
            tmp = vector[end];
            vector[end] = vector[pos];
            vector[pos++] = tmp;

        }

        n = pos;
    }

    return vector[0];
}

void partition(int64_t* vector, int64_t n, int64_t pivot, int64_t smaller) {
    int64_t pos = 0;
    int64_t i = 0;
    int64_t tmp = 0;

    for (i = 0; i < n; i++) {
        if ((smaller && vector[i] < pivot) || (!smaller && vector[i] > pivot)) {
            tmp = vector[i];
            vector[i] = vector[pos];
            vector[pos++] = tmp;
        }
    }
    n = pos;
}

int64_t* countEle(int64_t* vector, int64_t n, int64_t pivot) {
    int64_t values[2] = {0, 0};
    int64_t* v = &values[0];
    int64_t i = 0;

    for (i = 0; i < n; i++) {
        if (vector[i] < pivot) {
            values[0]++;
        } else if (vector[i] > pivot) {
            values[1]++;
        }
    }

    return v;
}

/** TODO
 * Returns the lower median
 *
 * [1 2 3] ===> 2
 * [1 2 3 4] => 2
 */
int64_t get_median(int64_t* vector) {
    
    if (g_length <= 1) {
        return vector[0];
    }
    /*
    int64_t* sorted = cloned(vector);

    sort(sorted, 0);
    int64_t median = sorted[medianIndex];

    free(sorted);
    return median;
    */

    int64_t* clone = cloned(vector);
    int64_t value = 0;
    int64_t pivot = 0;
    int64_t* count = 0;
    int64_t n = g_length;

    while (true) {
        pivot = findPivot(vector, n);

        count = countEle(clone, n, pivot);

        if (medianIndex < count[0]) {
            partition(clone, n, pivot, 1);
        } else if (medianIndex < n - count[1]) {
            value = pivot;
            break;
        } else {
            medianIndex -= n - count[1];
            partition(clone, n, pivot, 0);
        }
    }

    free(clone);
    return value;
}

/**
 * Returns the minimum and maximum of a vector
 */
int64_t* get_values(int64_t* vector) {
    // min, max
    int64_t values[2] = {vector[0], vector[0]};
    int64_t* ptr = &values[0];

    if (g_length <= 1) {
        return ptr;
    }

    int64_t i = 0;

    for (i = 1; i < g_length; i++) {
        if (vector[i] < values[0]) {
            values[0] = vector[i];
        } else if (vector[i] > values[1]) {
            values[1] = vector[i];
        }
    }

    return ptr;

}

/** TODO
 * Returns the smallest value in the vector
 *
 * [1 2 3] => 1
 * [2 3 1] => 1
 */
int64_t get_minimum(int64_t* vector) {
    return get_values(vector)[0];
}

/** TODO
 * Returns the largest value in the vector
 *
 * [1 2 3] => 3
 * [2 3 1] => 3
 */
int64_t get_maximum(int64_t* vector) {
    return get_values(vector)[2];
}

/**
 * Returns the frequency of the value in the vector
 */
int64_t get_frequency(int64_t* vector, int64_t value) {
    int64_t count = 0;
    int64_t i = 0;

    for (i = 0; i < g_length; i++) {
        if (vector[i] == value) {
            count += 1;
        }
    }

    return count;
}

/**
 * Returns the value stored at the given element index
 */
int64_t get_element(int64_t* vector, int64_t index) {

    return vector[index];
}

/**
 * Output given vector to standard output
 */
void display(int64_t* vector, int64_t label) {

    printf("vector~%" PRId64 " ::", label);
    int64_t i = 0;

    for (i = 0; i < g_length; i++) {
        printf(" %" PRId64, vector[i]);
    }

    puts("");
}

