#include <stdio.h>
#include <stdlib.h>

#define MAXNUM 100

/*
 * Calculates all primes up to MAXNUM using a prime sieve
 */

int main(void) {
    char *isprime = calloc(MAXNUM, sizeof(char));

    for (int i = 2; i*i <= MAXNUM; i++) {
        if (isprime[i] == 0) {
            for (int j = i*i; j < MAXNUM; j += i) {
                isprime[j] = 1;
            }
        }
    }

    for (int i = 2; i < MAXNUM; i++) {
        if (isprime[i] == 0) {
            printf("%d, ", i);
        }
    }
    printf("\n");

}
