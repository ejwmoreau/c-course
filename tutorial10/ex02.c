#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

// Matrices here are WIDTH x WIDTH
#define WIDTH (512)
#define NTHREADS (4)
#define DIV (WIDTH / NTHREADS)

/*
 * Performs matrix multiplication on two large matrices
 * - Task was to parallelise multiply()
 */

typedef struct {
    double *inA;
    double *inB;
    double *out;
    int start, end;
} WorkerArgs;

void *worker(void *arg) {
    WorkerArgs *wargs = (WorkerArgs *) arg;
    for (int i = wargs->start; i < wargs->end; i++) {
        for (int j = 0; j < WIDTH; j++) {
            wargs->out[WIDTH * i + j] = 0;
            for (int k = 0; k < WIDTH; k++) {
                wargs->out[WIDTH * i + j] += wargs->inA[WIDTH * i + k] * wargs->inB[WIDTH * k + j];
            }
        }
    }

    return NULL;
}

// Set outC = inA x inB.
void multiply(double *inA, double *inB, double *outC) {
    WorkerArgs wargs[NTHREADS];
    pthread_t threads[NTHREADS];

    for (int i = 0; i < NTHREADS; i++) {
        wargs[i].inA = inA;
        wargs[i].inB = inB;
        wargs[i].out = outC;
        wargs[i].start = i * DIV;
        wargs[i].end = (i + 1) * DIV;
        printf("Start: %d, End: %d\n", wargs[i].start, wargs[i].end);
    }

    for (int i = 0; i < NTHREADS; i++) {
        pthread_create(&threads[i], NULL, worker, (void *) &wargs[i]);
    }

    for (int i = 0; i < NTHREADS; i++) {
        pthread_join(threads[i], NULL);
    }
}

double trace(double *A) {
    double sum = 0;
    for (int i = 0; i < WIDTH; i++) {
        sum += A[WIDTH * i + i];
    }
    return sum;
}

int main(void) {
    // Create three matrices.
    double *A = calloc(WIDTH * WIDTH, sizeof(double));
    double *B = calloc(WIDTH * WIDTH, sizeof(double));
    double *C = calloc(WIDTH * WIDTH, sizeof(double));

    // Create initial values for A and B
    for (int i = 0; i < WIDTH; i++) {
        for (int j = 0; j < WIDTH; j++) {
            A[WIDTH * i + j] = i + j;
            B[WIDTH * i + j] = i - j;
        }
    }

    // Set C = A x B
    multiply(A, B, C);

    // Print out the trace of the result.
    printf("%f\n", trace(C));

    free(A);
    free(B);
    free(C);
    return 0;
}
