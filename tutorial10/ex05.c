#include <stdio.h>

/*
 * Will only show the secret message if buf is overflowed and then 'DCBA' is inputted
 */

int main(void) {
  int target = 0;
  char buf[128];

  printf("%p\n%p\n", &target, buf);
  printf("What is your name? ");
  scanf("%s", buf);

  if (target == 0x41424344) {
    puts("You found the secret!");
    puts("<secret message goes here>");
  } else {
    printf("Nice to meet you, %s.\n", buf);
  }

  return 0;
}
